import {uglify} from 'rollup-plugin-uglify';

const config = (file, plugins) => ({
    input: 'src/index.js',
    output: {
        name: 'kdbush',
        format: 'umd',
        indent: false,
        file
    },
    plugins
});

export default [
    config('dist/kdbush.js', []),
    config('dist/kdbush.min.js', [uglify()])
];
